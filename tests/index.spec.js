// Importamos el módulo en el que creamos el endopoint a testear

import app from '../src/app'
import request from 'supertest'

describe('GET /task', () => {

 test('Should respond with a 200 status code', async () => {
    const response = await request(app).get('/tasks').send();
    expect(response.statusCode).toBe(200);
 });
 test('Should respond with an array', async () => {
    const response = await request(app).get('/tasks').send();
    expect(response.body).toBeInstanceOf(Array);
    });
});

describe('POST /tasks', () => {
   describe('Given a tittle and description', () => {
      
   const newTask = {
      tittle: "Test task",
      description : "Test description"
   }
   
   test('Should respond with a 200 status code', async () => {
      const response = await request(app).post('/tasks').send(newTask);
      expect(response.statusCode).toBe(200);
   });


   test('Should have a content-type: application/json in header', async () => {
      const response = await request(app).post('/tasks').send(newTask);
      expect(response.headers['content-type']).toEqual(
         expect.stringContaining("json")
      );
   });

   test('Should respond with a task id', async () => {
      const response = await request(app).post('/tasks').send(newTask);
      expect(response.body.id).toBeDefined();
      });
   });

   describe('When the tittle and description are missing', () => {
      test('Should respond with a 400 status code', async () => {
         const fields = [
            {},
            {tittle : 'Test task'},
            {description: 'Test descripction'},
         ]

         for (const body of fields) {
            const response = await request(app).post('/tasks').send(body);
            expect(response.statusCode).toBe(400);
         }
      });
   });
});