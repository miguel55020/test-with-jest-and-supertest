// Aquí crearemos nuestro servidor || configuración del servidor 

import express, { request } from 'express';
import {v4} from 'uuid';

const app = express();
app.use(express.json());

//Crearemos un endpoint, es decir una simple url que podamos visitar 
app.get('/ping', (req, res) => {
    res.send('pong')
});

app.get('/tasks', (req, res) =>{
    res.json([]);
})

app.post('/tasks', (req, res) => {
    const {tittle, description} = req.body;

    if(!tittle || !description) return res.sendStatus(400);

    res.json({
        tittle,
        description,
        id: v4()
    });
});
// Eportamos el servidor
export default app;

// el res.send devuelve un text/html es decir devuelve un html y un string dentro del html